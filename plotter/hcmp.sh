#!/bin/bash -e

file1=$1
file2=$2

fout=$(mktemp)
fscript=$fout.script

cat >$fscript <<EOF

# BEGIN PLOT
textField1=file1 $file1
textField2=file2 $file2
drawRatioPlot=1
outputFileName=$fout
# END PLOT

# BEGIN HISTOGRAM
filename=$file1
reference=1
markerStyle=21
markerSize=1
lineStyle=1
lineWidth=0
color=1 0 0
legend=file1
# END HISTOGRAM

# BEGIN HISTOGRAM
filename=$file2
reference=0
markerStyle=21
markerSize=1
lineStyle=1
lineWidth=0
color=0 1 0
legend=file2
# END HISTOGRAM
EOF

./plotter.exe chi2=5 $fscript
./plotter.exe $fscript

echo "Chi2 = $(cat $fout.txt | cut -d';' -f1)"

ls -l $fout.*
