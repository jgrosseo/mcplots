# based on example from Herwig 7.0.4 distribution:
#   src/LHC-mb.in

# Run options:
cd /Herwig/Generators
set LHCGenerator:NumberOfEvents %nevts%
set LHCGenerator:RandomNumberGenerator:Seed %seed%
set LHCGenerator:DebugLevel 0
set LHCGenerator:PrintEvent 1
set LHCGenerator:MaxErrors 1000000

# redirect all log output to stdout
set LHCGenerator:UseStdout true

# do output to a HepMC file
cd /Herwig/Generators
insert LHCGenerator:AnalysisHandlers 0 /Herwig/Analysis/HepMCFile
set /Herwig/Analysis/HepMCFile:PrintEvent 1000000
set /Herwig/Analysis/HepMCFile:Format GenEvent
set /Herwig/Analysis/HepMCFile:Filename %outfile%
# set /Herwig/Analysis/HepMCFile:Units GeV_mm

# Beam parameters:
set LHCGenerator:EventHandler:LuminosityFunction:Energy %energy%
set LHCGenerator:EventHandler:BeamA /Herwig/Particles/%beamA%
set LHCGenerator:EventHandler:BeamB /Herwig/Particles/%beamB%
set LHCGenerator:MaxErrors -1

# MinBias process:
cd /Herwig/MatrixElements/
insert SimpleQCD:MatrixElements[0] MEMinBias

# MPI model settings
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
cd /Herwig/Generators

# Change to have no pT cuts for MinBias
set LHCGenerator:EventHandler:Cuts /Herwig/Cuts/MinBiasCuts

# Make particles with c*tau > 10 mm stable:
set /Herwig/Decays/DecayHandler:MaxLifeTime 10*mm
set /Herwig/Decays/DecayHandler:LifeTimeOption Average

# tune '%tune%' parameters: -------------------
#%tuneFile%
# ---------------------------------------------

# Run generator
cd /Herwig/Generators

# Fix to get correct cross section
create Herwig::MPIXSecReweighter MPIXSecReweighter
insert LHCGenerator:EventHandler:PostSubProcessHandlers 0 MPIXSecReweighter

run TVT LHCGenerator
