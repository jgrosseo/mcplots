# steering file based on example from Sherpa 1.2.3 distribution:
#   share/SHERPA-MC/Examples/LHC_ZJets/Run.dat

(run){
  # disable colorizing of text printed on screen:
  PRETTY_PRINT = Off
  
  # number of events:
  EVENTS = %nevts%
  
  # set random seed:
  # (see section "6.1.3 RANDOM_SEED" of Sherpa manual for details)
  RANDOM_SEED = %seeds%
  
  # Event output file:
  HEPMC2_GENEVENT_OUTPUT = sherpa
  # full name of output file will be:
  #  "sherpa.hepmc2g"
  
  # disable splitting of HepMC output file:
  FILE_SIZE = 1000000000
  
  # Makes particles with c*tau > 10 mm stable:
  MAX_PROPER_LIFETIME = 10.0
}(run)

(beam){
  BEAM_1 =  %beam1%; BEAM_ENERGY_1 = %beamEn%;
  BEAM_2 =  %beam2%; BEAM_ENERGY_2 = %beamEn%;
}(beam)

# Switch on W->e,nu and W->mu,nu plus at least one jet, + CKKW merging to two jets
# Allow slightly lower precision on 4-parton cross sections
(processes){
  Process 93 93 -> 11 -12 93 93{1}
  Order_EW 2;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.01 {3};
  Integration_Error 0.02 {4};

  Process 93 93 -> -11 12 93 93{1}
  Order_EW 2;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.01 {3};
  Integration_Error 0.02 {4};

  Process 93 93 -> 13 -14 93 93{1}
  Order_EW 2;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.01 {3};
  Integration_Error 0.02 {4};

  Process 93 93 -> -13 14 93 93{1}
  Order_EW 2;
  CKKW sqr(30/E_CMS)
  Integration_Error 0.01 {3};
  Integration_Error 0.02 {4};

  End process;
}(processes)

(selector){
  # Set cuts
  # Use this for hard leading QCD parton in a certain pT window
  PT 93 %pTmin% %pTmax%

  # Sherpa allows to constrain the dilepton mass; use rather large window (RIVET does the rest)
  Mass -11  12 40. E_CMS
  Mass  11 -12 40. E_CMS
  Mass -13  14 40. E_CMS
  Mass  13 -14 40. E_CMS

}(selector)

(me){
  ME_SIGNAL_GENERATOR = Internal Comix
}(me)

(mi){
  MI_HANDLER = Amegic  # None or Amisic
}(mi)

# Parameter specifications for '%tune%': -------------------
#%tuneFile%
# ---------------------------------------------
