#include "YODA/IO.h"

#include <iostream>
#include <string>
using namespace std;

int main(int argc, char* argv[])
{
  if (argc < 2) {
    cerr << "Usage: yoda2flat-split.exe [file1.yoda ...]\n"
         << "Convert .yoda (YODA format) files to .dat ('flat' format) files\n"
         << "Output is one .dat file per each histogram" << endl;
    return 1;
  }
  
  // iterate over input files
  for (int i = 1; i < argc; i++) {
    const string infile = argv[i];
    const vector<YODA::AnalysisObject*> aos = YODA::read(infile);
    
    if (aos.size() == 0) {
      cerr << "ERROR: file " << infile << " does not exist or empty" << endl;
      return 1;
    }
    
    // iterate over histograms inside file
    for (size_t j = 0; j < aos.size(); j++) {
      const YODA::AnalysisObject& ao = *aos[j];
      string path = ao.path().substr(1);
      
      // replace '/' -> '_'
      while (true) {
        const size_t p = path.find("/");
        if (p == string::npos) break;
        path.replace(p, 1, "_");
      }
      
      YODA::write(path + ".dat", ao);
    }
  }
  
  return 0;
}
