#!/bin/bash

# attach plot description data to histogram .dat file from corresponding .plot file from Rivet data directory

fname="$1"

if [[ "$fname" == "" ]] ; then
  echo "ERROR: missing file name"
  exit 1
fi

if grep -q "# BEGIN PLOT" $fname ; then
  echo "ERROR: file $fname already completed"
  exit 1
fi

# extract histogram 'path'
histo=$(grep -m 1 Path= $fname)
histo=${histo#*=}
histo=${histo// }
histo=${histo#/REF}

# get analysis name
analysis=${histo#/}
analysis=${analysis%%/*}

# find corresponding .plot file
data=$RIVET_ANALYSIS_PATH/$analysis.plot
if ! test -e $data ; then
  data=$RIVET_REF_PATH/$analysis.plot
  if ! test -e $data ; then
    echo "ERROR: failed to find .plot file for $analysis"
    exit 1
  fi
fi

#echo "fname = $fname"
#echo "histo = $histo"
#echo "analysis = $analysis"
#echo "data = $data"

# parse .plot file and extract all blocks which correspond to $histo
{
echo "# BEGIN PLOT $histo"

while read line ; do
  if [[ "$line" =~ "BEGIN PLOT" ]] ; then
    # current line is PLOT block start
    pattern=${line#*BEGIN PLOT}
    pattern=${pattern// }
    
    if [[ "$histo" =~ $pattern ]] ; then
      # block match histogram $histo, enable output of content
      print=1
      continue
    fi
  fi
  
  if [[ "$line" =~ "END PLOT" ]] ; then
    print=0
    continue
  fi
  
  if [[ "$print" == "1" ]] ; then
    echo "$line"
  fi
done

echo "# END PLOT"
} < $data >> $fname
