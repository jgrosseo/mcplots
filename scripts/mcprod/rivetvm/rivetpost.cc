#include "Rivet/AnalysisHandler.hh"
#include "Rivet/Analysis.hh"
#include "Rivet/Tools/RivetYODA.hh"

#include "HepMC/GenEvent.h"
#include <fstream>
#include "tools.h"

using namespace std;

int main(int argc, char* argv[])
{
  // Vector with Rivet analyses
  vector<string> analyses;
  
  // HepMC input file
  string input_file = "input.hepmc";
  
  // .dat histograms output directory
  string output_dat_dir = ".";
  
  // YODA output file
  string output_yoda = "output.yoda";
  
  // YODA input files for postprocessing
  vector<string> yoda_input_files;
  
  // Iterate over command-line arguments
  for (int i = 1; i < argc; i++) {
    const string par = argv[i];
    const string val = (i < argc - 1) ? argv[i+1] : "";
    
    if (par == "--list-analyses") {
      listAvailableAnalyses();
      return 0;
    }
    else if (par == "-a") {
      analyses.push_back(val);
    }
    else if (par == "-o") {
      output_dat_dir = val;
    }
    else if (par == "-H") {
      output_yoda = val;
    }
    else if (par == "-Y") {
      yoda_input_files.push_back(val);
    }
  }
  
  // Check if analyses have been supplied
  if (analyses.size() == 0) {
    cerr << "ERROR: no analysis specified" << endl;
    return 1;
  }
  
  // Check analyses names
  foreach (const string& i, analyses) {
    if (!checkAnalysis(i)) {
      cerr << "ERROR: requested analysis " << i << " is not available" << endl;
      return 1;
    }
  }
  
  // Rivet
  Rivet::AnalysisHandler rivet;
  rivet.addAnalyses(analyses);
  
  // check if there are input yoda files
  if (yoda_input_files.empty()) {
    cerr << "ERROR: there are no input yoda files for postprocessing!" << endl;
    return 1;
  }
  // read input yoda files for postprocessing
  for (auto yoda_input_file : yoda_input_files) {
    rivet.readData(yoda_input_file);
  }
  // start postprocessing mode
  // initialize run with a dummy event to pass sanity checks
  HepMC::GenVertex* vtx1 = new HepMC::GenVertex;
  vtx1->add_particle_in(new HepMC::GenParticle);
  vtx1->add_particle_in(new HepMC::GenParticle);
  HepMC::GenEvent evt;
  evt.add_vertex(vtx1);
  rivet.init(evt);
  // finalize a run
  rivet.finalize();
  // start post method
  rivet.post();
  // write out histograms
  dumpState(output_dat_dir, rivet);
  // dump all histograms to .yoda file
  rivet.writeData(output_yoda);

  return 0;
}
