#include "YODA/WriterYODA.h"
#include "YODA/ReaderYODA.h"
#include "YODA/Histo1D.h"

#include <iostream>
#include <string>

using namespace std;

int main(int argc, char* argv[])
{
  
  string inputFile = "/data/mcplots/mcplots2/scripts/mcprod/input.yoda";
  string outputFile = "/data/mcplots/mcplots2/scripts/mcprod/calibration/output.yoda";
  string analysisName = "ANALYSIS_NAME";
  
  string nevts = "default";
  
  for (int i = 1; i < argc; ++i) {
    
    const string par = argv[i];
    const string val = (i < argc - 1) ? argv[i+1] : "";
    
    if ( par == "-i" ) {
      inputFile = val;
    } else if ( par == "-o" ) {
      outputFile = val;
    } else if ( par == "-a" ) {
      analysisName = val;
    }
  }
  
  // read data from input file  
  YODA::Reader &reader = YODA::mkReader( inputFile );
  ifstream in( inputFile.c_str() );
  
  if( !in ) {
    cerr << "Failed to open file " << inputFile ;
    exit(1);
  }
  vector< YODA::AnalysisObject* > aos;
  
  reader.read(in, aos);
  in.close();
  
  if ( aos.size() == 0 ) {
    cerr << "ERROR: file " << inputFile << " does not exist or empty" << endl;
    return 1;
  }
  
  std::cout << analysisName << ": extracting calibration and control histograms from " << inputFile << std::endl;
  // search for calibration histograms
  vector< YODA::Histo1D* > aosOut;
  for( YODA::AnalysisObject* ao : aos ) {
    std::string tmpPath = ao->path();
    if((tmpPath.find(analysisName) != std::string::npos) && (tmpPath.find("calib") != std::string::npos)) {
      // search for corresponding control histogram
      tmpPath.replace(tmpPath.find("calib"), std::string("calib").length(), "control");
      for (YODA::AnalysisObject* ao2 : aos) {
	if (ao2->path() == tmpPath) {
	  // check if object type is supported
	  if ((ao->type() != "Histo1D") || (ao2->type() != "Histo1D")) {
	    std::cout << analysisName << ": object type " << ao->type() << " or " << ao2->type() << " not supported. Skipping..." << std::endl;
	    continue;
	  }
	  // merge calibration and control histograms
	  std::cout << analysisName << ": merging " << ao->path() << " and " << ao2->path() << std::endl;
	  YODA::Histo1D* aohist = dynamic_cast<YODA::Histo1D*>(ao);
	  YODA::Histo1D* aohist2 = dynamic_cast<YODA::Histo1D*>(ao2);
	  (*aohist)+=(*aohist2);
	  aosOut.push_back(aohist);
	}
      }
    }
  }
  std::cout << analysisName << ": extracted " << aosOut.size() << " calibration histogram(s)" << std::endl;

  
  // write output
  if (aosOut.size() > 0) {
    YODA::WriterYODA::write( outputFile , aosOut.begin(), aosOut.end() );
    std::cout << analysisName << ": calibration histogram(s) saved to " << outputFile << std::endl;
  }
  
  return 0;
}
