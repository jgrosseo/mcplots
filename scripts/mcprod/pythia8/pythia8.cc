// Pythia8 wrapper (based on example main32.cc from Pythia8 distribution)
// Vincia wrapper (based on vincia01.cc example from Vincia distribution)

#ifdef VINCIA
#if VIVERSION < 2000
#include "Vincia.h"
#else
#include "Vincia/Vincia.h"
using namespace Vincia;
#endif
#endif

#if VERSION < 180
#include "Pythia.h"
#include "HepMCInterface.h"
#elif VERSION < 200
#include "Pythia8/Pythia.h"
#include "Pythia8/Pythia8ToHepMC.h"
#else
#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/HepMC2.h"
#endif

#include "HepMC/GenEvent.h"
#include "HepMC/IO_GenEvent.h"
#ifdef HEPMC_HAS_UNITS
#include "HepMC/Units.h"
#endif

using namespace Pythia8;

int main(int argc, char* argv[])
{
  // Check that correct number of command-line arguments
  if (argc != 3) {
    cerr << "ERROR: Unexpected number of command-line arguments.\n"
         << "       Expected one input and one output file name" << endl;
    return 1;
  }
  
  // Print command-line parameters
  cout << "Input steering file = " << argv[1] << "\n"
       << "Output HepMC file = " << argv[2] << endl;
  
  // Check that the input steering file exists
  ifstream is(argv[1]);
  if (!is) {
    cerr << "ERROR: input steering file does not found" << endl;
    return 1;
  }
  
  // Interface for conversion from Pythia8::Event to HepMC one
#if VERSION < 180
  HepMC::I_Pythia8 ToHepMC;
#else
  HepMC::Pythia8ToHepMC ToHepMC;
#endif
  //  ToHepMC.set_crash_on_problem();
  
  // Specify file where HepMC events will be stored
  HepMC::IO_GenEvent ascii_io(argv[2], std::ios::out);
  
  // Generator
  Pythia pythia;
  
#ifdef VINCIA
  // get location of the Vincia configuration data
  // the VINCIADATA variable is supported by Vincia itself starting from version 1.0.25
  // and this part of code is neccessary for older versions < 1.0.25
  const char* xmldir = getenv("VINCIADATA");
  if (!xmldir) {
    cerr << "ERROR: VINCIADATA environment variable (path to xmldoc/) is not set" << endl;
    return 1;
  }
  
  // define plug-in
  VinciaPlugin vincia(&pythia, argv[1], xmldir);
  // activate debug print
  //pythia.readString("Vincia:verbose = 9");
  
  // TODO: New VINCIA initialisation structure from 2.2.00
#else
  
  // Read in commands from external file
  pythia.readFile(argv[1]);
#endif
  
  if (pythia.info.errorTotalNumber() > 0) {
    cerr << "ERROR: error proccessing steering file" << endl;
#if VERSION < 219
    pythia.info.errorStatistics(cerr);
#else 
    pythia.info.errorStatistics();
#endif
    return 1;
  }
  
  // Extract settings to be used in the main program
  int    nEvent    = pythia.mode("Main:numberOfEvents");
  bool   isLHE     = (pythia.mode("Beams:frameType") == 4);
  int    nShow     = 0;
  int    nAbort    = nEvent;
  bool   showA     = false;
  bool   showC     = true;
  
  // initialization
  if (! pythia.init()) return 1;
  
  // List settings
  if (showC) pythia.settings.listChanged();
  if (showA) pythia.settings.listAll();
  
  // List particle data
  // suppress changed particle data printing for LHE input
  if (showC && !isLHE) pythia.particleData.listChanged();
  if (showA) pythia.particleData.listAll();
  
  // Begin event loop
  int nPace  = max(1, nEvent / max(1, nShow) );
  int iAbort = 0;
  
  for (int i = 0; i < (nEvent + iAbort); ++i) {
    if (nShow > 0 && (i % nPace == 0))
      cout << " Now begin event " << i << endl;
    
    // Generate event
    if (!pythia.next()) {
      
      if (pythia.info.atEndOfFile()) {
        // end of input LHEF file is reached
        cout << "INFO: stop event generation since reached end of Les Houches Event File" << endl;
        break;
      }
      
      // record failure and skip the event
      iAbort++;
      
      // First few failures write off as "acceptable" errors, then quit.
      if (iAbort < nAbort) continue;
      
      cerr << "ERROR: abort event generation due to high number of errors" << endl; 
      return 1;
    }
    
    // Construct new empty HepMC event
    #ifdef HEPMC_HAS_UNITS
      HepMC::GenEvent* hepmcevt = new HepMC::GenEvent(HepMC::Units::GEV, HepMC::Units::MM);
    #else
      HepMC::GenEvent* hepmcevt = new HepMC::GenEvent();
    #endif
    
    // fill HepMC event
    ToHepMC.fill_next_event(pythia, hepmcevt);
    
    // write the HepMC event to file
    ascii_io << hepmcevt;
    delete hepmcevt;
  }
  
  // print event generation statistics
#if VERSION < 200
  pythia.statistics();
#else
  pythia.stat();
#endif
  
#ifdef VINCIA
  vincia.printInfo();
#endif
  
  return 0;
}
