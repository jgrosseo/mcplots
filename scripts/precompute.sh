#!/bin/bash

#IF NO ARGUMENTS WERE PROVIDED
function USAGE ()
{
    echo "Script to compute statistic, so first user will not wait too long."
    echo ""
    echo "USAGE: "
    echo "    precompute.sh [-?] servername"
    echo ""
    echo "OPTIONS:"
    echo "    -?  this usage information"
    echo ""
    echo "EXAMPLE:"
    echo "    precompute.sh "
    echo "    precompute.sh mcplots-dev "
    echo ""
    exit $E_OPTERROR    # Exit and explain usage, if no argument(s) given.
}

#PROCESS ARGS
while getopts "?" Option
do
    case $Option in
        ?    ) USAGE
               exit 0;;
        *    ) echo ""
               echo "Unimplemented option chosen."
               USAGE   # DEFAULT
    esac
done

shift $(($OPTIND - 1))
#  Decrements the argument pointer so it points to next argument.
#  $1 now references the first non option item supplied on the command line
#+ if one exists.

#Check time
start_time=`date +%s`

#first parameter
server=$1
if [[ $server == "" ]] ; then
  server=`uname -n`
fi


#recursive download of validation pages
#-A - accept only valid
#-R - reject others
#-l 1 - only first level - means that links in page ...?query=valid will be crawled not further
#--delete-after - delete files after downloading 
#-nd - while we don't need files we don't need to create directory structure as well
#-nv - no so much verbosity
wget -r -A "*valid*" -R "*validdetail*",txt,css,png,news,gensAndVers,frontpage,html -l 1 --delete-after -nd -nv http://$server/?query=valid

end_time=`date +%s`
echo execution time was `expr $end_time - $start_time` s.

rm -rf $tmpdir
