#!/bin/bash -e

# Script `update-production.sh` is intended to be run as cron job.
# Manual edditing of script at the time of execution can lead to
# dangerous results.
# The purpose of this wrapper is to protect agains this case by
# picking up and executing the latest version of script from the code repository.

# path to script in code repository:
svnscript="https://gitlab.cern.ch/MCPLOTS/mcplots/raw/master/scripts/update-production.sh"


echo "$(date) Exporting production script..."

# temporal local copy of script:
script=$(mktemp -t update-production.sh.XXXXXXXX)
wget -nv -O $script $svnscript
chmod +x $script

$script

rm -f $script
