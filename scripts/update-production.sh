#!/bin/bash

# path to SVN repository of MC production machinery:
svnrepo="http://svn.cern.ch/guest/mcplots/trunk/scripts/mcprod"
svnjob="http://svn.cern.ch/guest/mcplots/trunk/scripts/job.run"
svnplotter="http://svn.cern.ch/guest/mcplots/trunk/plotter"
svnmerge="http://svn.cern.ch/guest/mcplots/trunk/scripts/mcprod/merge"

# path to MC production machinery cache on CVMFS:
# (used to decrease job size)
cvmfsrepo="/cvmfs/sft.cern.ch/lcg/external/mcplots"

# paths to copilot shared directories:
# jobs input queue:
inputQueue="/opt/copilot/input"
# jobs output queue:
outputQueue="/opt/copilot/output /opt/copilot/output_condor /opt/copilot/output_native"
# copilot state: internal queue length, number of connected machines
copilotState="/opt/copilot/state"

# threshould on queue length in order to submit new jobs (for LXBATCH and BOINC):
minQueueLen="1100"

# place to put results:
pool="/home/mcplots/pool"

# number of events per job:
maxEventsPerJob="100000"
minEventsPerJob="1000"
eventsDropRate="7"

# parameter to specify how often to submit runs with the 'latest' version of
# generator as compared with the 'older' versions of a generator to collect
# statistics faster for the newest one
latestVersionRate="2"


# ==============================================================================

# wrapper function to make queries to local database
function dosql() {
  mysql --no-defaults --skip-column-names -u mcplots mcplots
}

# check all input parameters are unsigned integers
function test_uint() {
  for x in "$@" ; do
    if ! test "$x" -ge 0 2>/dev/null ; then
      return 1
    fi
  done
  
  return 0
}

# query T4T API to resolve BOINC USERID by AUTHENTICATOR
function query_t4t_userid() {
  local auth="$1"
  local api="http://lhcathome2.cern.ch/vLHCathome/show_user.php?format=xml&auth"
  
  xmlstarlet sel --net -t -v /user/id "$api=$auth" 2>/dev/null
}

function init_db() {
  {
    # table of revisions of production machinery:
    echo "CREATE TABLE IF NOT EXISTS production"
    echo "("
    echo "  revision     INT      NOT NULL PRIMARY KEY,"   # machinery revision number
    echo "  seed         INT      NOT NULL,"               # current seed of random number generator
    echo "  lxbatch      INT      NOT NULL,"               # flag to run production on LXBATCH
    echo "  boinc        INT      NOT NULL,"               # flag to run production on BOINC
    echo "  added        DATETIME NOT NULL"                # time of revision date
    echo ");"
    
    # table of runs specifications
    echo "CREATE TABLE IF NOT EXISTS runs"
    echo "("
    echo "  id           INT       NOT NULL PRIMARY KEY AUTO_INCREMENT,"
    echo "  revision     INT       NOT NULL,"
    echo "  run          CHAR(200) NOT NULL,"
    echo "  events       BIGINT    NOT NULL,"
    echo "  attempts     INT       NOT NULL,"
    echo "  success      INT       NOT NULL,"
    echo "  failure      INT       NOT NULL,"
    echo "  UNIQUE INDEX (revision, run)"
    echo ");"
    
    # jobs db
    echo "CREATE TABLE IF NOT EXISTS jobs"
    echo "("
    echo "  id        INT       UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,"  # job ID
    echo "  revision  SMALLINT  UNSIGNED NOT NULL,"    # job revision
    echo "  runid     INT       UNSIGNED NOT NULL,"    # run ID
    echo "  subdate   DATETIME           NOT NULL,"    # job submission date
    echo "  retdate   DATETIME,"                       # job return date
    echo "  system    TINYINT   UNSIGNED NOT NULL,"    # batch system: 0 = Unknown, 1 = vLHC@home, 2 = vLHCdev@home, 3 = LHC@home
    echo "  cpuusage  INT       UNSIGNED,"             # job CPU usage, seconds
    echo "  diskusage INT       UNSIGNED,"             # job disk usage, kilobytes
    echo "  events    MEDIUMINT UNSIGNED NOT NULL,"    # number of events
    echo "  seed      MEDIUMINT UNSIGNED NOT NULL,"    # initial seed of random number generator
    echo "  exitcode  TINYINT   UNSIGNED,"             # job exit code
    echo "  userid    MEDIUMINT UNSIGNED,"             # BOINC user ID
    echo "  hostid    INT       UNSIGNED,"             # BOINC host ID
    echo "  INDEX (revision, runid),"
    echo "  INDEX (system, userid, hostid)"
    echo ");"
    
    # telemetry log:
    echo "CREATE TABLE IF NOT EXISTS telemetry"
    echo "("
    echo "  id        INT      NOT NULL PRIMARY KEY AUTO_INCREMENT,"  # event ID
    echo "  date      DATETIME NOT NULL,"                             # event date and time
    echo "  revision  INT      NOT NULL,"                             # revision to which the event belongs to
    echo "  param     CHAR(50) NOT NULL,"                             # parameter name
    echo "  value     INT      NOT NULL,"                             # parameter value
    echo "  INDEX (revision, param)"
    echo ");"
    
  } | dosql
}

function record_telemetry() {
  local revision="$1"
  local key="$2"
  local value="$3"
  
  echo "INSERT INTO telemetry VALUES ( NULL, NOW(), $revision, '$key', $value );" | dosql
}


# this function syncronize 'production' table with revisions
# of MC production machinery from SVN
function fetch_new_svn_revisions() {
  echo "[$(date)] -> fetch_new_svn_revisions()"
  
  echo "Quering $svnrepo ..."
  
  # get list of revisions numbers and dates from SVN:
  local svnlog=$(svn log -q $svnrepo | grep ^r | cut -d ' ' -f 1,5,6 | sed 's,^r,,' | sort -n)
  echo "Total svn revisions = $(echo "$svnlog" | wc -l)"
  
  echo "Updating DB ..."
  
  {
    echo "LOCK TABLES production WRITE;"
    
    echo "$svnlog" | while read rev date time ; do
      # add new revision to table (skip if revision is already here):
      echo "INSERT IGNORE INTO production VALUES ( $rev, 0, 0, 0, '$date $time' );"
    done
    
    echo "UNLOCK TABLES;"
  } | dosql
  
  echo ""
}


# this function updates 'runs' table with run specifications of
# revisions selected for production
function prepare_runs_list () {
  echo "[$(date)] -> prepare_runs_list()"
  
  # get list of revisions marked for production:
  local rev=$(echo "SELECT revision FROM production WHERE (lxbatch = 1 OR boinc = 1) AND revision NOT IN (SELECT DISTINCT revision FROM runs)" | dosql | xargs)
  
  if [[ "$rev" == "" ]] ; then
    echo "No new revisions to prepare runs list"
    echo ""
    
    return
  fi
  
  echo "Preparing runs list for revisions: $rev"
  echo "Updating DB ..."
  
  for i in $rev ; do
    local tmp=$(mktemp -d)
    svn export --force -q -r $i $svnrepo $tmp || continue
    
    # take into account increased number of parameters starting from revision #761
    local specf="2-8"  # before #761, run spec. situated in fields 2-8
    if (( "$i" >= 761 )) ; then
      specf="2-9"      # starting from #761, run spec. in 2-9
    fi
    
    { echo "LOCK TABLES runs WRITE;"
      
      # make list of all runs, cut 'run specification' part of each entry
      (cd $tmp && ./runAll.sh list xxx) | cut -d ' ' -f "$specf" | while read run ; do
        # and add run specification to the 'runs' table:
        echo "INSERT IGNORE INTO runs VALUES ( NULL, $i, '$run', 0, 0, 0, 0 );"
      done
      
      echo "UNLOCK TABLES;"
    } | dosql
    
    record_telemetry $i "runs-list-added" 1
    
    rm -rf $tmp
  done
  
  echo ""
}


# function check copilot input queue and return:
#  0 - queue is full or not reachable - no need to refill
#  1 - queue is empty and reachable   - to be refilled
function check_copilot_input_queue_len () {
  echo "Checking copilot public input queue..."
  
  # get list of jobs in the input queue
  local pubQueueList
  pubQueueList=$(find $inputQueue -ignore_readdir_race -type f -name '*.run')
  local pubEnumCode=$?
  
  # length of copilot public jobs queue:
  local pubQueueLen
  
  if [[ "$pubQueueList" != "" ]] ; then
    pubQueueLen=$(echo "$pubQueueList" | wc -l)
  else
    # work-around to correctly calculate size of empty queue
    pubQueueLen="0"
  fi
  
  if [[ "$pubEnumCode" != "0" ]] ; then
    # sometimes `find` prints error messages and exit with non-zero exit code while enumeration the input queue directory
    #   find: `/opt/copilot/input/2279-800864-38.run': No such file or directory
    # probably the reason is the removal of job files by batch system at the time of enumeration
    # still, the file list looks correct, so just issue the warning and keep the processing
    
    echo "WARNING: listing of copilot input queue $inputQueue returns non-zero exit code = $pubEnumCode"
    #return 0
  fi
  
  echo "length ($inputQueue) = $pubQueueLen"
  record_telemetry 0 "copilot-jobs-queue-len" $pubQueueLen
  
  echo "Checking copilot internal queues..."
  
  # length of copilot internal queues:
  local intQueuesLen="0"
  local intAccessOK="1"
  
  # NOTE: the copilot is stop since 2017Apr21
  #for f in $(find $copilotState -type f -name "jm*") ; do
  #  local len=$(cat $f)
  #  echo "length ($f) = $len"
  #  
  #  if [[ "$len" != "" ]] ; then
  #    let intQueuesLen+=len
  #  else
  #    intAccessOK="0"
  #  fi
  #done
  
  if [[ "$intAccessOK" == "1" ]] ; then
    # total length:
    local totalLen=$((pubQueueLen + intQueuesLen))
    
    echo "total input queue length (public + internal) = $pubQueueLen + $intQueuesLen = $totalLen"
    record_telemetry 0 "copilot-jobs-queue-total" $totalLen
  else
    echo "WARNING: failed to access copilot internal queues $copilotState/jmX"
  fi
  
  if (( "$pubQueueLen" > "$minQueueLen" )) ; then
    echo "Copilot public queue is full ($pubQueueLen > $minQueueLen)"
    echo ""
    return 0
  fi
  
  # queue is empty, return 1 to trigger refill
  return 1
}

# check the recent jobs fail or lost rate are below threshold
function check_recent_jobs_problem_rate() {
  echo "Checking recent problem rate..."
  
  # TODO: add error checking for DB queries
  
  local interval="4:05"  # 4 hours (+5 minutes to accomodate script startup jitter)
  local threshold="50"   # 50%
  echo "interval = $interval"
  echo "threshold = $threshold %"
  
  # get initial lenght of the copilot input queue
  local len=$(echo "SELECT value FROM telemetry WHERE param='copilot-jobs-queue-total' AND date < (NOW() - interval '$interval' hour_minute) ORDER BY id DESC" | dosql | head -n 1)
  echo "len = $len"
  
  # get number of submitted and received (good and bad) jobs
  local q=$(echo "SELECT SUM((param = 'submit-to-boinc')*value), SUM((param = 'good-jobs')*value), SUM((param = 'bad-jobs')* value) FROM telemetry WHERE date > (NOW() - interval '$interval' hour_minute)" | dosql)
  local a=( $q )
  local nsub=${a[0]}
  local ngood=${a[1]}
  local nbad=${a[2]}
  echo "nsub = $nsub"
  echo "ngood = $ngood"
  echo "nbad = $nbad"
  
  # get current lenght of the copilot input queue
  local len0=$(echo "SELECT value FROM telemetry WHERE param='copilot-jobs-queue-total' ORDER BY id DESC LIMIT 1" | dosql)
  echo "len0 = $len0"
  
  local nrcv=$((ngood + nbad))
  local ndrain=$((nsub + len - len0))
  local nlost=$((ndrain - nrcv))
  echo "nrcv = $nrcv"
  echo "ndrain = $ndrain"
  echo "nlost = $nlost"
  
  # calculate fail and lost rate
  local failrate="0"
  if [[ "$nrcv" != "0" ]] ; then
    failrate=$((100 * nbad / nrcv))
  fi
  
  local lostrate="0"
  if [[ "$ndrain" != "0" ]] ; then
    lostrate=$((100 * nlost / ndrain))
  fi
  
  echo "failrate = $failrate %"
  echo "lostrate = $lostrate %"
  
  if (( "$failrate" > "$threshold" )) ; then
    echo "WARNING: high fail rate"
    return 1
  fi
  
  if (( "$lostrate" > "$threshold" )) ; then
    echo "WARNING: high lost rate"
    return 1
  fi
  
  # problem rate is normal
  return 0
}

# swap fields 1 and 2 in the input stream
function swap12 () {
  while read a b ; do
    echo $b $a
  done
}

function submit_boinc_jobs() {
  echo "[$(date)] -> submit_boinc_jobs()"
  
  # check input queue and exit if queue is full
  check_copilot_input_queue_len && return 0
  
  # limit number of new jobs if fail or lost rate are too high
  local jobsLimit=""
  if ! check_recent_jobs_problem_rate ; then
    echo "INFO: job limit is set to 100"
    jobsLimit="LIMIT 100"
  fi
  
  # get revisions selected for production on BOINC:
  local rev=$(echo "SELECT revision FROM production WHERE boinc = 1" | dosql | xargs)
  
  echo "Revisions to submit to BOINC: $rev"
  
  for i in $rev ; do
    echo "@ $(date +%s.%N) start"
    echo "Submitting revision $i ..."
    
    # get and increase SEED counter
    local seed=$(echo "SELECT seed FROM production WHERE revision = $i" | dosql)
    echo "UPDATE production SET seed = seed + 1 WHERE revision = $i" | dosql
    
    echo "seed = $seed"
    
    # prepare paths to temporary directories and files
    local tmpd="$(mktemp -d)"
    local prodd="$tmpd/$i"
    local pack="$tmpd/$i.tgz"
    local tmplJob="$tmpd/job.run"
    local srcJob="$tmpd/job0.run"
    
    # download latest version of job template:
    svn export -q $svnjob $tmplJob || continue
    
    # init job file
    touch $srcJob
    chmod a+x $srcJob
    
    # create tarball with production machinery:
    svn export -q -r $i $svnrepo $prodd || continue
    svn export -q -r $i $svnplotter $prodd/plotter || continue
    
    local packdist="$cvmfsrepo/$i.tgz"
    local packsrc=""
    
    # check if tarball is cached already in CVMFS
    # and it was created/modified more than 1 day (24*60 minutes) ago
    # (enough time to propagate to all CVMFS clients)
    if [[ -f $packdist && "$(find $packdist -mmin +1440)" != "" ]] ; then
      # check cached tarball contents really matches to exported SVN contents
      local cvmfsprodd="$(mktemp -d)"
      tar -xzf $packdist -C $cvmfsprodd
      
      if diff -rq $prodd $cvmfsprodd >& /dev/null ; then
        # cache is valid
        packsrc=$packdist
      else
        echo "WARNING: $packdist does not match SVN revision $i"
      fi
      
      rm -rf $cvmfsprodd
    fi
    
    echo "packsrc = $packsrc"
    
    if [[ "$packsrc" == "" ]] ; then
      # remove unnecessary files to decrease job size
      rm -rf $prodd/merge $prodd/runAll.sh $prodd/plotter/data
      
      tar -czf $pack -C $prodd . || continue
    else
      # integrate empty binary into job
      echo -n > $pack
    fi
    
    echo "Uploading jobs ..."
    echo ""
    
    local filter=""
    
    if (( seed % latestVersionRate != 0 )) ; then
      # get list of the latest versions of generators
      filter=$(echo "SELECT run FROM runs WHERE revision = $i" | dosql | cut -d ' ' -f 6,7 | sort -ru | swap12 | uniq -f 1 | swap12)
    fi
    
    # get total number of already submitted jobs:
    local totalAttempts1=$(echo "SELECT SUM(attempts) FROM runs WHERE revision = $i" | dosql)
    
    # get average number of lost jobs per run:
    local lost_avg=$(echo "SELECT AVG(attempts - success - failure) from runs where revision = $i and attempts > 0" | dosql)
    
    echo "@ $(date +%s.%N) sub_init"
    
    echo "SELECT attempts, success + failure, id, run FROM runs WHERE revision = $i ORDER BY RAND() $jobsLimit" | dosql | grep -F "$filter" | while read nsub nrcv runid run ; do
      echo "@ $(date +%s.%N) start"
      
      # $nsub  - number of submited jobs
      # $nrcv  - number of received jobs
      # $runid - run ID
      # $run   - run specification
      
      # exclude following runs:
      #   "alpgenpythia6" gives "Segmentation fault" on i686
      #   "alpgenherwigjimmy" does not run (some problem with mcprod machinery)
      if [[ "${run/alpgen/}" != "$run" ]] ; then
        echo "Skipping '$run'"
        continue
      fi
      
      # TODO: resolve alpgen* problems
      
      echo "@ $(date +%s.%N) sub_job_filtr"
      
      # Estimate number of events to submit according to amount of lost jobs.
      # Use $maxEventsPerJob events if #lost jobs below threshould:
      #   lost_jobs < sqrt(submitted_jobs) + <lost_jobs>
      #     lost_jobs = attempts - (success + failure)
      #     <lost_jobs> - average number of lost jobs over all runs
      #
      # , else:
      #     index = lost_jobs - (sqrt(submitted_jobs) + <lost_jobs>)
      #     nevt = min_jobs + (max_jobs - min_jobs) * exp(- rate * index)
      
      local nevt="$maxEventsPerJob"
      
      if [[ "$nsub" != "0" ]] ; then  # check submitted_jobs != 0
        local ind=$(echo "($nsub - $nrcv - sqrt($nsub) - $lost_avg)/$nsub" | bc -l)
        
        if [[ ${ind:0:1} != "-" ]] ; then  # check do we above threshould
          nevt=$(echo "$minEventsPerJob + ($maxEventsPerJob - $minEventsPerJob) * e(- $eventsDropRate * $ind)" | bc -l)
          
          # round to 1000
          nevt=$(echo "$nevt / 1000 * 1000" | bc)
          
          if (( "$nevt" < "$minEventsPerJob" )) ; then
            nevt="$minEventsPerJob"
          fi
        fi
      fi
      
      echo "@ $(date +%s.%N) sub_job_init"
      
      local system="0"
      
      # create new job record in DB and extract job ID
      local jobid=$(echo "INSERT INTO jobs VALUES ( NULL, $i, $runid, NOW(), NULL, $system, NULL, NULL, $nevt, $seed, NULL, NULL, NULL ); SELECT LAST_INSERT_ID()" | dosql)
      
      if [[ "$jobid" == "" ]] ; then
        echo "ERROR: failed to add job record to DB, exit from job submission loop"
        break
      fi
      
      echo "@ $(date +%s.%N) sub_job_db_ins"
      
      # cook job file:
      local runspec="boinc $run $nevt $seed"
      { sed -e "s/%runspec%/$runspec/" \
            -e "s/%run%/$run/" \
            -e "s,%jobid%,$jobid," \
            -e "s,%revision%,$i," \
            -e "s,%runid%,$runid," \
            -e "s,%system%,$system," \
            -e "s,%events%,$nevt," \
            -e "s,%seed%,$seed," \
            -e "s,%packsrc%,$packsrc," \
            $tmplJob
        cat $pack
      } > $srcJob
      
      echo "@ $(date +%s.%N) sub_job_file"
      
      # put job to copilot input queue:
      local dstJob="$inputQueue/$i-$runid-$seed.run"
      echo "Submitting '$runspec' ($dstJob)"
      cp $srcJob "$dstJob"
      
      echo "@ $(date +%s.%N) sub_job_queue"
      
      if [[ "$?" == "0" ]] ; then
        # increase number of submits
        echo "UPDATE runs SET attempts = attempts + 1 WHERE id = $runid" | dosql
      else
        # remove job record
        echo "DELETE FROM jobs WHERE id = $jobid" | dosql
        
        echo "ERROR: failed to submit job, possible network connection to copilot is lost"
        break # exit from job submission loop
      fi
      
      echo "@ $(date +%s.%N) sub_job_db_done"
    done
    
    echo "@ $(date +%s.%N) start"
    
    # get new total number of submitted jobs:
    local totalAttempts2=$(echo "SELECT SUM(attempts) FROM runs WHERE revision = $i" | dosql)
    
    # record number of just submitted jobs:
    record_telemetry $i "submit-to-boinc" $((totalAttempts2-totalAttempts1))
    
    rm -rf $tmpd
    
    echo "@ $(date +%s.%N) sub_fini"
  done
  
  # re-check input queue after submit to record queue length telemetry
  check_copilot_input_queue_len
  
  echo ""
}

# NOTE: the copilot is stop on 2017Apr21, the function is not called anymore
# collect various telemetry exported by copilot host:
function get_copilot_stats() {
  echo "[$(date)] -> get_copilot_stats()"
  
  local listFile="$copilotState/volunteers"
  
  # each minute copilot appends file $listFile by
  # the current number of online agents (connected machines)
  
  # get the list
  local nConnectedList
  nConnectedList=$(cat $listFile)
  
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: failed to read $listFile"
    return 0
  fi
  
  echo -n "Connected agents history: "
  echo "$nConnectedList" | paste -s -d ,
  
  # current number of connected machines:
  local nMachines=$(echo "$nConnectedList" | tail -n 1)
  
  if [[ "$nMachines" == "" ]] ; then
    echo "ERROR: history is empty"
    return 0
  fi
  
  # average/min/max number of machines over the period from last update:
  local avgMachines=$(echo "$nConnectedList" | awk '{if (NF > 0) {n++; s+=$1}} END {if (n > 0) {print int(0.5 + s/n)} else {print 0}}')
  local minMachines=$(echo "$nConnectedList" | sort -n | head -n 1)
  local maxMachines=$(echo "$nConnectedList" | sort -n | tail -n 1)
  
  # try to reset history:
  echo -n > $listFile
  
  if [[ "$?" != "0" ]] ; then
    echo "WARNING: failed to reset $listFile"
  fi
  
  echo "Connected machines: current / average / min / max = $nMachines / $avgMachines / $minMachines / $maxMachines"
  record_telemetry 0 "copilot-connected-machines" $avgMachines
  
  echo ""
}


function get_boinc_results() {
  echo "[$(date)] -> get_boinc_results()"
  
  echo "Listing finished jobs..."
  
  # list all files in results directory:
  local jobsList
  jobsList=$(find $outputQueue -type f -name '*.tgz')
  
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: failed to access copilot output queue $outputQueue"
    return 1
  fi
  
  local finishedLen=$(echo "$jobsList" | wc -l)
  
  # work-around to correctly calculate size of empty queue
  if [[ "$jobsList" == "" ]] ; then
    finishedLen="0"
  fi
  
  echo "Current number of copilot done jobs = $finishedLen"
  record_telemetry 0 "copilot-done-queue-len" $finishedLen
  
  # checkout and build merge tool
  echo "Preparing merge tool..."
  local mergedir=$(mktemp -d)
  svn export -q --force $svnmerge $mergedir
  make -B -C $mergedir
  
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: failed to prepare merge tool. Skipping jobs processing."
    return 1
  fi
  
  echo "Downloading finished jobs..."
  
  # counters for numbers of succesfull and failed jobs:
  # indeces (or keys) correspond to revisions, and values correspond to counters
  local n_good=( )
  local n_bad=( )
  local cpu_good=( )
  local cpu_bad=( )
  local n_corrupt="0"
  local n_dup="0"
  
  # limit number of jobs to process to have better progress indication on production page
  # TODO: fix hardcoded number
  jobsList=$(echo "$jobsList" | head -n 2700)
  
  # copy/move jobs to local machine:
  for i in $jobsList ; do
    echo "@ $(date +%s.%N) start"
    echo "Processing $i"
    
    local tmpd="$(mktemp -d)"
    local fname=$(basename $i)
    
    # move job from copilot to local host
    mv $i $tmpd/$fname
    
    if [[ "$?" != "0" ]] ; then
      echo "WARNING: failed to get the job. Skip current job."
      rm -rf $tmpd
      continue
    fi
    
    echo "@ $(date +%s.%N) move"
    
    # job integrity flag, zero if no problems detected
    local integrityStatus="0"
    
    # unpack job
    # option '--exclude $fname' is a protection to prevent
    # over-writting of original job file if file with the
    # same name exists inside archive $fname
    tar zxmf $tmpd/$fname -C $tmpd --exclude $fname
    integrityStatus="$?"
    
    echo "@ $(date +%s.%N) unpack"
    
    # process job metadata:
    # line-terminating characters in BOINC_* strings could be CRLF / LF / CR
    # depending on the volonteer's system
    # replace CR by LF to fix strings procesing on mcplots-dev (SLC)
    sed 's,\r,\n,' -i $tmpd/jobdata
    
    # debug output:
    echo -n "jobdata: "
    xargs < $tmpd/jobdata
    
    # read metadata
    local jobid=$(sed -n "s,^jobid=\(.*\)$,\1,p" $tmpd/jobdata)
    local rev=$(sed -n "s,^revision=\(.*\)$,\1,p" $tmpd/jobdata)
    local runid=$(sed -n "s,^runid=\(.*\)$,\1,p" $tmpd/jobdata)
    local cpuusage=$(sed -n "s,^cpuusage=\(.*\)$,\1,p" $tmpd/jobdata)
    local diskusage=$(sed -n "s,^diskusage=\(.*\)$,\1,p" $tmpd/jobdata)
    local nevt=$(sed -n "s,^events=\(.*\)$,\1,p" $tmpd/jobdata)
    local exitcode=$(sed -n "s,^exitcode=\(.*\)$,\1,p" $tmpd/jobdata)
    local userid=$(sed -n "s,^BOINC_USERID=\(.*\)$,\1,p" $tmpd/jobdata)
    local hostid=$(sed -n "s,^BOINC_HOSTID=\(.*\)$,\1,p" $tmpd/jobdata)
    
    # sanity check (unsigned integer)
    if ! test_uint "$jobid" "$rev" "$runid" "$cpuusage" "$diskusage" "$nevt" "$exitcode" ; then
      integrityStatus="1"
    fi
    
    # sanity check (unsigned integer)
    if ! test_uint "$userid" ; then userid="NULL" ; fi
    if ! test_uint "$hostid" ; then hostid="NULL" ; fi
    
    # the default is copilot/vLHC system (the BOINC_PROJECT metadata is not present)
    local system="1"
    
    # condor jobs have BOINC_PROJECT metadata to distinguish between various @home projects
    if grep -q "^BOINC_PROJECT=" $tmpd/jobdata ; then
      local project=$(sed -n "s,^BOINC_PROJECT=\(.*\)$,\1,p" $tmpd/jobdata)
      
      case "$project" in
        "vLHC"    ) system="1" ;;
        "vLHCdev" ) system="2" ;;
        "LHC"     ) system="3" ;;
        *         ) system="0" ;;
      esac
      
      echo "condor-job: jobid=$jobid project=$project -> system=$system"
    fi
    
    echo "@ $(date +%s.%N) metadata"
    
    # TODO: recovery works now only for vLHC project
    if [[ "$system" == "1" && "$userid" == "0" ]] ; then
      # BOINC clients 6.10.x always return USERID = 0
      # try to recover USERID based on AUTHENTICATOR
      
      local auth=$(sed -n "s,^BOINC_AUTHENTICATOR=\(.*\)$,\1,p" $tmpd/jobdata)
      userid=$(query_t4t_userid "$auth")
      
      # check the value is valid (unsigned integer)
      if ! test_uint "$userid" ; then userid="0" ; fi
      
      # debug output:
      echo "recovery-userid: 0 -> $userid"
      
      echo "@ $(date +%s.%N) uidrecov"
    fi
    
    if [[ "$integrityStatus" != "0" ]] ; then
      echo "WARNING: job is corrupted."
      
      # copy full job file for investigation
      mkdir -p $pool/failed/unknown
      cp -f $tmpd/$fname $pool/failed/unknown/
      
      # count corrupted job
      let n_corrupt+=1
      
      rm -rf $tmpd
      
      echo "@ $(date +%s.%N) corrupt"
      
      continue
    fi
    
    # at this point the job integrity is ok
    
    if [[ "$exitcode" == "0" ]] ; then
      # job finished successfully, copy results (histograms) to pool:
      mkdir -p $pool/$rev/dat
      
      if (( "$rev" >= 857 )) ; then
        # merge new histograms into central repository
        ( cd $mergedir && ./merge.sh $tmpd/dat $pool/$rev/dat ) > /dev/null
      else
        # old revisions do not support merging, just do a simple copy
        cp -rf $tmpd/dat $pool/$rev/
      fi
      
      echo "@ $(date +%s.%N) goodmerge"
      
      # increase number of successes and events
      echo "UPDATE runs SET success = success + 1, events = events + $nevt WHERE id = $runid AND revision = $rev" | dosql
      
      echo "@ $(date +%s.%N) gooddb"
      
      # increase good jobs counter and CPU usage:
      let n_good[rev]+=1
      let cpu_good[rev]+=cpuusage
      
      #echo "Job succeeded: $fname (see results in $pool/$rev/)"
    
    else
      # job failed, make a copy of log file for investigation:
      mkdir -p $pool/failed/$rev
      cp -f $tmpd/runRivet.log $pool/failed/$rev/$fname.log
      
      echo "@ $(date +%s.%N) badmerge"
      
      # increase number of failures
      echo "UPDATE runs SET failure = failure + 1 WHERE id = $runid AND revision = $rev" | dosql
      
      echo "@ $(date +%s.%N) baddb"
      
      # increase bad jobs counter and CPU usage:
      let n_bad[rev]+=1
      let cpu_bad[rev]+=cpuusage
      
      echo "Job failed"
    fi
    
    echo "@ $(date +%s.%N) jobdone"
    
    # update job record
    local nchanged=$(echo "UPDATE jobs SET retdate = NOW(), cpuusage = $cpuusage, diskusage = $diskusage, exitcode = $exitcode, system = $system, userid = $userid, hostid = $hostid WHERE id = $jobid AND retdate IS NULL; SELECT ROW_COUNT()" | dosql)
    
    if [[ "$nchanged" == "0" ]] ; then
      echo "WARNING: duplicate job"
      let n_dup+=1
    fi
    
    echo "@ $(date +%s.%N) jobdb"
    
    rm -rf $tmpd
    
    echo "@ $(date +%s.%N) clean"
  done
  
  rm -rf $mergedir
  
  # --- record counters of good and bad jobs:
  # (iterate over revisions)
  for i in ${!n_good[@]}; do record_telemetry $i "good-jobs" ${n_good[$i]} ; done
  for i in ${!n_bad[@]};  do record_telemetry $i "bad-jobs"  ${n_bad[$i]}  ; done
  
  # no good jobs at this check:
  if [[ "${#n_good[@]}" == "0" ]] ; then
    record_telemetry 0 "good-jobs" 0
  fi
  
  # no bad jobs at this check:
  if [[ "${#n_bad[@]}" == "0" ]] ; then
    record_telemetry 0 "bad-jobs" 0
  fi
  
  # --- record jobs CPU usage:
  for i in ${!cpu_good[@]}; do record_telemetry $i "good-cpuusage" ${cpu_good[$i]} ; done
  for i in ${!cpu_bad[@]};  do record_telemetry $i "bad-cpuusage"  ${cpu_bad[$i]}  ; done
  [[ "${#cpu_good[@]}" == "0" ]] && record_telemetry 0 "good-cpuusage" 0
  [[ "${#cpu_bad[@]}"  == "0" ]] && record_telemetry 0 "bad-cpuusage"  0
  
  # corrupted and duplicated jobs
  record_telemetry 0 "corrupt-jobs" $n_corrupt
  record_telemetry 0 "dup-jobs" $n_dup
  
  # maintenance: remove old and empty folders from copilot output directory
  find $outputQueue -mindepth 1 -type d -mtime +3 -empty -delete
  
  echo ""
}


#
function refresh_api_cache() {
  echo "[$(date)] -> refresh_api_cache()"
  
  {
    # create new api cache data table
    echo "DROP TABLE IF EXISTS api_new;"
    echo "CREATE TABLE api_new"
    echo "("
    echo "  date1G  DATE,"
    echo "  date10G DATE,"
    echo "  UNIQUE INDEX (system, userid, hostid)"
    echo ")"
    echo "SELECT"
    echo "  system,"
    echo "  userid,"
    echo "  hostid,"
    echo "  SUM(cpuusage) AS cpu_time,"
    echo "  SUM(diskusage) AS disk_usage,"
    echo "  SUM((exitcode != 0)*cpuusage) AS cpu_bad,"
    echo "  SUM((exitcode = 0)*events) AS n_events,"
    echo "  COUNT(*) AS n_jobs,"
    echo "  SUM(exitcode = 0) AS n_good_jobs,"
    echo "  SUM(retdate > (NOW() - INTERVAL 1 MONTH)) AS n_jobs_1m,"
    echo "  SUM((exitcode = 0)*(retdate > (NOW() - INTERVAL 1 MONTH))) AS n_good_jobs_1m"
    echo "FROM jobs"
    echo "WHERE retdate IS NOT NULL"
    echo "GROUP BY 1, 2, 3;"
    
    echo "CREATE TABLE IF NOT EXISTS api LIKE api_new;"
    
    # copy 1G and 10G achievements dates to new cache
    # (these dates remain constant once reached, thus no need to recalculate)
    echo "UPDATE"
    echo "  api_new, api"
    echo "SET"
    echo "  api_new.date1G = api.date1G,"
    echo "  api_new.date10G = api.date10G"
    echo "WHERE"
    echo "  api_new.system <=> api.system AND api_new.userid <=> api.userid;"
    
    # NOTE: to invalidate the date1G/date10G cache run manually:
    # $ echo "UPDATE api SET date1G = NULL, date10G = NULL" | mysql -u mcplots mcplots
    
    # swap new cache with old cache
    echo "RENAME TABLE api TO api_old, api_new TO api;"
    
    # drop old cache
    echo "DROP TABLE IF EXISTS api_old;"
  } | dosql
  
  # calculate date1G and date10G
  for i in 1 10 ; do
    local f="date${i}G"
    local n="${i}000000000"
    
    # get the list of users who just reach the achievement
    local users=$(echo "SELECT system, userid, $f, SUM(n_events) as total FROM api GROUP BY 1, 2 HAVING total >= $n AND $f <=> NULL" | dosql | cut -f 1,2 --output-delimiter=: | xargs)
    
    echo "New ${i}G users: $users"
    
    for user in $users ; do
      local s=${user%%:*}
      local u=${user##*:}
      
      echo "Processing $user (system=$s, userid=$u)..."
      
      {
        echo "UPDATE api, ("
        echo "  SELECT"
        echo "    *"
        echo "  FROM ("
        echo "    SELECT"
        echo "      *, (@s := @s + events) AS total"
        echo "    FROM ("
        echo "      SELECT @s := 0"
        echo "    ) AS vars,"
        echo "    ("
        echo "      SELECT"
        echo "        DATE(retdate) AS date,"
        echo "        SUM((exitcode = 0)*events) AS events"
        echo "      FROM"
        echo "        jobs"
        echo "      WHERE"
        echo "        system <=> $s AND userid <=> $u"
        echo "      GROUP BY"
        echo "        1"
        echo "    ) AS q1"
        echo "    WHERE"
        echo "      date IS NOT NULL"
        echo "  ) AS q2"
        echo "  WHERE"
        echo "    total >= $n"
        echo "  LIMIT"
        echo "    1"
        echo ") AS achi"
        echo "SET"
        echo "  api.$f = achi.date"
        echo "WHERE"
        echo "  api.system <=> $s AND api.userid <=> $u"
      } | dosql
    done
  done
  
  echo ""
}


# record script busy time
function record_busy_time() {
  echo "[$(date)] -> record_busy_time()"
  
  # calculate and record spent CPU time:
  local tmpf=$(mktemp)
  times > $tmpf
  local busyCPUTime=$(cat $tmpf | xargs | sed -e 's,m,*60+,g' -e 's,s ,+,g' -e 's,s$,+0.5,' | bc | cut -d . -f 1)
  rm -f $tmpf
  
  # work-around if busyCPUTime < 1 second
  if [[ "$busyCPUTime" == "" ]] ; then
    busyCPUTime="0"
  fi
  
  echo "Script busy time (CPU) = $busyCPUTime s"
  record_telemetry 0 "busy-time" $busyCPUTime
  
  # elapsed wall clock time:
  local startTime="$1"
  
  if [[ "$startTime" != "" ]] ; then
    local stopTime=$(date +%s)
    local busyWallTime=$((stopTime - startTime))
    
    echo "Script busy time (wall) = $busyWallTime s"
    record_telemetry 0 "busy-time-wall" $busyWallTime
  fi
  
  echo ""
}

# make production page more responsive for first visitor by triggering cache renew
function update_prod_page_cache() {
  echo "[$(date)] -> update_prod_page_cache()"
  
  wget -q --spider \
    http://localhost/production.php?view=status \
    http://localhost/production.php?view=control \
    http://localhost/production.php?view=contrib
  
  echo ""
}

# === main ===

# TODO: implement reporting to know that the script is working or not

# lock in order to prevent execution of several copies of script
mkdir -p $pool/
lockFile=$pool/lock

# check the lock file exists and process created the file is running
if [[ -e $lockFile ]] && kill -0 $(cat $lockFile) ; then
  echo "Script $0 already running. Skipping execution and exit."
  exit 0
fi

echo $$ > $lockFile

startTime=$(date +%s)

init_db

fetch_new_svn_revisions
prepare_runs_list

submit_boinc_jobs

# copilot stop on 2017Apr21
#get_copilot_stats

get_boinc_results

refresh_api_cache

record_busy_time $startTime

update_prod_page_cache

echo "[$(date)] Update finished"
echo ""
echo ""

# release lock:
rm -f $lockFile
