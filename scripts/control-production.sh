#!/bin/bash -e

function show_usage () {
  echo "Usage: control-production.sh <command>"
  echo "  commands:"
  echo "    state          - show production status"
  echo "    set <revision> - enable #revision for production"
}

function dosql () {
  mysql --skip-column-names -u mcplots mcplots
}

function show_state () {
  echo -n "Current production revision: "
  echo "select revision from production where boinc = 1" | dosql | xargs
  
  echo -n "Revisions available for production (last 100): $available"
  echo "select revision from production order by 1 desc limit 100" | dosql | xargs
}

function set_prod () {
  declare -i revision=$1
  
  echo "Setting BOINC production revision to $revision ..."
  
  echo "update production set boinc = 0 where boinc != 0" | dosql
  echo "update production set boinc = 1 where revision = $revision" | dosql
}

case $1 in
  "state" ) show_state ;;
  "set"   ) set_prod $2 ;;
  *       ) show_usage ;;
esac
