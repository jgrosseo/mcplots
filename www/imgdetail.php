<?php
  
  $fname=$q_observable; //warning reusing input variable
  $tblsrc=$q_tunegroup; //warning reusing input variable  (usually contains generator--tune--v1--v2
  $isData = ($q_ref == "data");
  
  //$q_beamgroup, $q_process, $q_generator, $q_tune, $q_version, $q_ref from index.php
  $dispproc = $c->name($q_process);
  $dispgen = $c->name($q_generator);
  
  $ltext = ($isData ? "" : "vs. $q_ref");
  echo "<h2>Detail for $dispgen - $q_tune tune $q_version $ltext</h2>\n";
  // echo $validationNote;
  
    $fsteer = "cache/plots/$fname.script";
    $img = "cache/plots/$fname";

    if (is_file($fsteer)){
      //get links to dat files and recover energy observable cuts data
      foreach (file($fsteer) as $line) {
        if (strpos($line, "filename") !== false) {
          list( , $link) = explode("=", $line, 2);
          $linkarr[] = $link;
        }
      }
      
      list(,$fbeam,$fproc,$fobser,$fcuts,$fenergy,$fref)=explode("/",$linkarr[0]);

      // skip image generation if it already exists in cache/
      if (! file_exists("$img.small.png")) {
        // execute plotter to prepare .eps and .pdf files
        exec("./plotter.exe " . escapeshellarg($fsteer) . " >> cache/plotter.log 2>&1");
        // and convert .eps to .png
        // the raster (.png) plot looks rough being produced
        // by plotter (ROOT), that is why we deside to use
        // additional step with `convert` utility
        exec("convert -density 100 " . escapeshellarg("$img.eps") . " " . escapeshellarg("$img.png"));
        exec("convert -density  50 " . escapeshellarg("$img.eps") . " " . escapeshellarg("$img.small.png"));
        
        // optimize .png file size (lossless, by 30-40%)
        exec("optipng -q -zs0 -f0 " . escapeshellarg("$img.png"));
        exec("optipng -q -zs0 -f0 " . escapeshellarg("$img.small.png"));
      }

      //print header for graph
      $eb = $fbeam . " @ " . $fenergy . " GeV";
      $id = str_replace(" @ ", "", $eb);
      $id = str_replace(" GeV", "", $id);
      echo "<h3 id=\"$id\"><a href=\"#$id\">$eb ($dispproc)</a></h3>\n";
      echo "<div>\n";
      echo "\n";

      // print cell with one plot
      echo "<div class=\"plot\">\n";
      echo "<h4>" . $c->name($fcuts,$fproc) . "</h4>\n";

      // print image
      echo "<div><a href=\"$img.png\"><img src=\"$img.small.png\"></a></div>\n";

      // print "caption"
      echo "<div class=\"caption\">\n";
      echo " <a href=\"$img.pdf\">[pdf]</a>\n";
      echo " <a href=\"$img.eps\">[eps]</a>\n";
      echo " <a href=\"$img.png\">[png]</a>\n";

      echo " <span class=\"caption-toggler\" onClick=\"togglespan(this)\">show details &rarr;</span><br>\n";
      echo " <span class=\"caption-items-hidden\">\n";

      //experiment info
      list($reference,)=explode(".",$fref);
      list($experiment,)=explode("_",$reference);
      echo "  [<a href=\"" . $linkarr[0] . "\">" . $experiment . "</a>] ";
      echo "<a href=\"". get_reflink($reference) . "\">reference</a><br>\n";

      //mc info
      $lvers = ($isData ? "" : "($q_version)");
      echo "  [<a href=\"" . $linkarr[1] . "\">" . $c->name($q_tune, $q_generator) . " $lvers</a>] ";
      echo "<a href=\"" . str_replace(".dat", ".params", $linkarr[1]) . "\">param</a><br>\n";

      if (!$isData){
        //mc info2
        echo "  [<a href=\"" . $linkarr[2] . "\">" . $c->name($q_tune, $q_generator) . " ($q_ref)</a>] ";
        echo "<a href=\"" . str_replace(".dat", ".params", $linkarr[2]) . "\">param</a><br>\n";
      }

      echo "  [<a href=\"$fsteer\">steer</a>]\n";

      echo " </span>\n";
      echo "</div>\n";

      echo "</div>\n";
      echo "\n";


      echo "</div>\n";
    }
    else{
      echo "<h3>File doesn't exist</h3>\n";
    }
?>

<br/>

<div>
<div id="fb-root"></div>
<script async src="//connect.facebook.net/en_US/all.js#xfbml=1"></script>
<div class="fb-like" data-layout="button_count"></div>
</div>
