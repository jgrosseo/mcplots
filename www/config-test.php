<!DOCTYPE html>
<html>

<head>
  <title>Config test</title>
  <style>
    table {
      border-collapse: collapse;
    }
    table, th, td {
      border: 1px solid black;
    }
  </style>
</head>

<body>

<h1>Config test</h1>

<p>
  <a href="#abbreviations">abbreviations</a>
  <a href="#styles">styles</a>
  <a href="#tunes">tunes</a>
  <a href="#beams">beams</a>
  <a href="#functions">functions</a>
</p>


<?php
  include "profiler.php";
  $t = new Profiler();
  
  // read config file
  include "config.php";
  $c = new Config("mcplots.conf");
  
  $t->stamp("config parsing");
  
  // print config data
  echo "<h2 id=\"abbreviations\">abbreviations</h2>\n";
  echo "<table>\n";
  foreach (array_keys($c->abbrs) as $key) {
    $abbr = $c->abbrs[$key];
    
    echo "<tr>";
    echo "  <td>" . $key . "</td>";
    echo "  <td>" . $abbr["submenu"] . "</td>";
    echo "  <td>" . $abbr["name"] . "</td>";
    echo "  <td>" . $abbr["plotter"] . "</td>";
    echo "</tr>\n";
  }
  echo "</table>\n";
  echo "\n";
  
  echo "<h2 id=\"styles\">styles</h2>\n";
  echo "<table>\n";
  foreach (array_keys($c->styles) as $key) {
    echo "<tr>";
    echo "  <td>" . $key . "</td>";
    echo "  <td>" . $c->styles[$key] . "</td>";
    echo "</tr>\n";
  }
  echo "</table>\n";
  echo "\n";
  
  echo "<h2 id=\"tunes\">tunes</h2>\n";
  echo "<table>\n";
  foreach (array_keys($c->tunegroups) as $key) {
    echo "<tr>  <td colspan=\"2\">" . $key . "</td> </tr>\n";
    
    $i = 0;
    foreach ($c->tunegroups[$key] as $tune) {
      echo "<tr>  <td>" . $i++ . "</td> <td>" . $tune . "</td> </tr>\n";
    }
  }
  echo "</table>\n";
  echo "\n";
  
  echo "<h2 id=\"beams\">beams</h2>\n";
  echo "<table>\n";
  foreach (array_keys($c->beamgroups) as $key) {
    echo "<tr>  <td colspan=\"2\">" . $key . "</td> </tr>\n";
    
    $i = 0;
    foreach ($c->beamgroups[$key] as $beam) {
      echo "<tr>  <td>" . $i++ . "</td><td>" . $beam . "</td> </tr>\n";
    }
  }
  echo "</table>\n";
  echo "\n";
  
  echo "<h2 id=\"functions\">functions</h2>\n";
  echo "<table>\n";
  echo "<tr>";
  echo "  <td>Config::name('pt-vs-mass','ue') =</td>";
  echo "  <td>" . $c->name("pt-vs-mass","ue") . "</td>";
  echo "</tr>\n";
  echo "<tr>";
  echo "  <td>Config::plotter('pt-vs-mass','ue') =</td>";
  echo "  <td>" . $c->plotter("pt-vs-mass","ue") . "</td>";
  echo "</tr>\n";
  echo "<tr>";
  echo "  <td>Config::submenu('pt-vs-mass','ue') =</td>";
  echo "  <td>" . $c->submenu("pt-vs-mass","ue") . "</td>";
  echo "</tr>\n";
  echo "</table>\n";
  echo "\n";
  
  $t->stamp("html generation");
  
  // print timing
  echo "<br>\n";
  echo "<hr>\n";
  echo "<p>Page generation took: " . $t->summaryLine() . "</p>\n";
?>

</body>

</html>
