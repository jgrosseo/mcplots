<html>

<head>
  <title>Databases and tables</title>
</head>

<body>

<h1>Databases and tables</h1>

<?php
  // function returns the list of databases and tables
  function list_tables() {
    $tables = array();
    
    // list all available databases
    $query = mysql_list_dbs();
    while ($row = mysql_fetch_object($query)) {
      $db = $row->Database;
      $tables[$db] = array();
    }
    
    // list tables in databases
    foreach (array_keys($tables) as $db) {
      mysql_select_db($db);
      $query = mysql_query("SHOW TABLES");
      while ($row = mysql_fetch_row($query)) {
        $tables[$db][] = $row[0];
      }
    }
    
    return $tables;
  }
  
  // display contents of the table
  function display_table($table) {
    $filter = "";
    switch ($table) {
      case "production": $filter = "ORDER BY revision DESC LIMIT 100"; break;
      case "runs"      : $filter = "ORDER BY id DESC LIMIT 100"; break;
      case "telemetry" : $filter = "ORDER BY id DESC LIMIT 100"; break;
      default:           $filter = "LIMIT 100"; break;
    }
    
    $query = mysql_query("SELECT COUNT(*) FROM $table $filter");
    $row = mysql_fetch_row($query);
    echo "<p>Total entries: $row[0]<p>\n";
    
    echo "<table border=1>\n";
    
    $query = mysql_query("SELECT * FROM $table $filter");
    
    // print columns names
    echo "<tr>\n";
    $ncols = mysql_num_fields($query);  // get number of fields in result
    for ($i = 0; $i < $ncols; $i++) {
      $name = mysql_field_name($query, $i);
      echo "  <th>$name</th>\n";
    }
    echo "</tr>\n";
    
    // print table contents
    while ($row = mysql_fetch_row($query)) {
      echo "<tr>\n";
      foreach ($row as $field) {
        echo "  <td><code>$field</code></td>\n";
      }
      echo "</tr>\n";
    }
    
    echo "</table>\n";
  }
  
  
  // open database
  if (!mysql_connect("localhost", "mcplots")) exit;
  
  // print list of databases and tables
  $list = list_tables();
  
  echo "<ul>\n";
  foreach ($list as $db => $tables) {
    echo "  <li>" . $db;
    
    // display list of tables only for 'mcplots' database
    if ($db == "mcplots") {
      echo "<br>\n";
      
      echo "  <ul>\n";
      foreach ($tables as $i) {
        echo "    <li><a href=\"?table=$i\">$i</a></li>\n";
      }
      echo "  </ul>";
    }
    
    echo "</li>\n";
  }
  echo "</ul>\n";
  echo "\n";
  
  // print table content
  $table = $_GET["table"];
  
  // check the table is exists to avoid hacks
  if (in_array($table, $list["mcplots"])) {
    echo "<h3>Table $table</h3>\n";
    mysql_select_db("mcplots");
    display_table($table);
    echo "\n";
  }
  
  mysql_close();
?>

</body>

</html>
