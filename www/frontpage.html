<h2>mcplots.cern.ch</h2>

<div id="team">
March 2018 - A. Karneyeu, <a href="http://skands.physics.monash.edu/">P. Skands</a>
</div>
<center>
<b>Reference</b>: Eur Phys J C74 (2014) 1
(<a href="http://arxiv.org/abs/1306.3436">arXiv:1306.3436</a>) 
</center>

<p><i>Note!</i> donate your unused CPU cycles to mcplots, via the
<a href="http://lhcathome.web.cern.ch/projects/test4theory">Test4Theory project</a>
(based on <a href="http://lhcathome.web.cern.ch/">LHC@home</a>).
</p>

<p><i>&larr; Select beam, process, and observable</i></p>


<div id="info">

<p>
<b>Navigate these pages</b> by using the menu to the left. 
The default for each topic is a comparison
of a small number of models to available data, but look for links at the top 
of each page for comparisons with more tunes/generators. 
Scroll down each page to see plots at 
other collider energies. To choose specific generator
version(s), use the
<a href="?query=versions">Generator Versions</a>
link towards the top of the menu (the default is to
just display the most recent ones).
More plots will be added, as new tunes become available, and as the
available data increases.
</p>

<p>
<b>Note:</b> <i>For a description in layman's terms, and/or to find out how to
donate your unused CPU cycles to help generate more statistics for
mcplots see the
<a href="http://lhcathome.web.cern.ch/projects/test4theory">Test4Theory project</a>,
the first volunteer cloud computing project to be based
on <a href="http://cernvm.cern.ch/">Virtual Machines</a> 
via the <a href="http://lhcathome.web.cern.ch/">LHC@home</a> platform.</i>
</p> 

<hr noshade size="5">


<h1>MCPLOTS</h1>

<p>
<b>MCPLOTS is intended</b> as a simple browsable repository of
MC (Monte Carlo) plots comparing High Energy Physics event generators
to a wide variety of available experimental data, 
for tuning and reference purposes. Apart from individual plots contained in papers
and presentations, there has not previously been any central database where people can 
quickly see how tune X of version Y of generator Z looks on distribution
D. The idea with mcplots is to provide such a repository, 
mainly based on the <a href="http://rivet.hepforge.org/">Rivet</a> analysis tool. 
</p>

<p>
<b>On each page</b>, selected by clicking in the menu on the left, you will
see a number of plots at different collider energies, comparing a set of
default generators/tunes to each other and to the data (if there is
any). If data for several different fiducial cuts are available at the
same energy, these will be arranged horizontally. At
the top of the page, you can select other generator/tune groups. The
default ("Main") is a comparison of the main generators. Each
additional tab then gives access to illustrations of more specialized
tunes/variations for each generator. 
</p>

<p>
<b>The millions of</b> different possible combinations of beam energies, cuts,
generators, versions, and tunes, means that a lot of attention has been paid
to how to simplify the site, in particular how to organize the site visually, 
and how to minimize the number of user clicks/inputs required to get what you
want. The current site is based on a prototype developed by P. Skands in the
context of workshops in Les Houches and Perugia
[<a href="http://arxiv.org/abs/0803.0678">LH08</a>,
<a href="http://arxiv.org/abs/1005.3457">Ska10</a>], but obviously
it also draws heavily on the lessons and tools developed as part of the
<a href="http://jetweb.hepforge.org/">JetWeb</a> 
project, in particular the <a href="http://rivet.hepforge.org/">Rivet</a> analysis tool.
The result is intended to look and feel 
much like any old-style browsable/clickable set of pages, 
with the more advanced  technologies it is based on having been hidden 'under the
hood', for the most part.  
</p>

<p>
<b>Ultimately, we aim</b> to include a possibility for more interactive use
of the site, with a special "USER" generator/tune group in which users can
compose their own comparisons and even upload their own files for
overplotting. 
</p>

<hr noshade size="5">


<h1>Missing an Interesting Plot / Tune / Generator?</h1>

<p><b>Write to P. Skands</b> (peter.skands.AT.monash.edu) if you are interested in
adding more analyses, distributions, generators, tunes, etc to this site.
We have endeavoured to make this as easy as possible. The steps are roughly as follows:</p>

<ul>
  <li>Start by checking out a copy of the mcplots SVN repository:
  <pre>svn co https://svn.cern.ch/reps/mcplots/trunk
    mcplots-trunk</pre> 
  This will allow you to experiment with modifying files locally, do
    test runs (most easily on CERN lxplus which has the required architecture in place)
  and communicate changes easily
  to the mcplots authors, using the output of <code>svn diff</code>.
</li>
<li>
If your plot is based on <a href="http://rivet.hepforge.org/">Rivet</a>, the
procedure is almost automatic, with the user just having to furnish a few
additional pieces of information; see section 4
in <a href="https://arxiv.org/pdf/1306.3436.pdf">arXiv:1306.3436</a>
for explicit instructions. Note: to keep
the mcplots site tidy and well organised, it is important to make judicious
use of the distinction between 'observables' and 'cuts'. The former
define what labels will appear in the left-hand menus, while the
latter will appear together as different plots on the same 'plot
plage', for the chosen observable label. 
As an example, for a set of
differential jet shape measurements using different pT cuts and different
jet algorithms / R values, we would normally use
'Differential Jet Shape' as the 'observable' for all of them
and let the different detailed definitions be referred to as
different 'cuts'; i.e., we use the word 'cut' in a general sense to
refer to different specific definitions of the same generic
'observable'. This avoids cluttering the left-hand menus up with
observable definitions that only differ by small details, and also
allows to see plots that only differ by such small details together
side by side on the same plot page.
</li>
<li>
If you want to add a new generator, this requires a little bit more
work. The automated computation back end and volunteer computing setup
make use of the LCG GENSER releases, so having the generator included
in such a release is normally the first step. See section 6 in
<a href="https://arxiv.org/pdf/1306.3436.pdf">arXiv:1306.3436</a>
for further explicit instructions. </li>
<li>To test if an updated set of MCPLOTS configuration files can be
  used to successfully generate events, you can use
  the <code>runRivet.sh</code> script in <code>local</code>
  or <code>lxplus</code> mode; see section 5
  in <a href="https://arxiv.org/pdf/1306.3436.pdf">arXiv:1306.3436</a></li>
for further instructions. </li>
<li>Finally, to test how updates to the database would look on the web
  site, you would need access to a php-enabled web server. Detailed
  instructions on how to do this are beyond the scope of this help
  text. Get in touch with the authors, and we can test your
  modifications eg on our mcplots-dev test server. </li>
</ul> 



<p>If you are having trouble, or if documentation is lacking for what you
  want to do, just write to us for information on how to get your
favourite plot / tune / generator up on this site.</p>


<h1>Problems with the Site?</h1>

<p><b>Write to A. Karneyeu</b> (anton.karneyeu.AT.cern.ch) if you are
experiencing technical problems with this site, or to report
problems with interpreting the contents of the site, such as menu
names, insufficiently labeled cuts or plots, ranges, etc.</p> 

<hr noshade size="5">


<h1>Support</h1>

<p>
The MCPlots site is being developed with participation by

<ul>
  <li>The <a href="http://wwwth.cern.ch/">CERN Theory Group</a></li>
  
  <li>The <a href="http://www.montecarlonet.org/">MCnet</a> Monte Carlo network</li>
  
  <li>CERN's <a href="http://sftweb.cern.ch/generators/">Generator Services project</a></li>
  
  <li>The <a href="http://cern.ch/LPCC/">CERN LHC Physics Center (LPCC)</a></li>
</ul>

<p><b>Funds may be available</b> to support
development and integration of new features / new analyses / new tools
on the site.</p>

<p>
<b>Write to P. Skands</b> 
(peter.skands.AT.monash.edu) if you have a project to propose and 
would be interested in a short-term stay at
CERN. 
</p>

</div>

<br/>
<br/>

<div align="center">
<div id="fb-root"></div>
<script async src="//connect.facebook.net/en_US/all.js#xfbml=1"></script>
<div class="fb-like" data-layout="button_count"></div>
</div>

