<h2>Generators and Versions</h2>

<p><b>The default on mcplots</b> is to display the most recent generator versions
that have so far been implemented on the site.
If that is all you want, you may start clicking in the menu on the left right away.</p>

<p><b>We also store</b> results generated with previous versions.
This page gives you the <i>possibility</i> to ask for some specific generator version(s)
(previous or current) to be displayed on the site.
This can be useful, for instance, if the default version for a particular generator on mcplots
is newer than what you are using for your analysis.</p>

<h3>How to choose your generator version(s)</h3>
<p><b>Below is a</b> table showing which generators are currently available on mcplots.
Leave the version selection links as "latest" if you just wish to see the most recent generator
version available for each plot (the default for mcplots).
If you wish instead to see only specific version(s) of each generator displayed on the site,
use the corresponding links to select which version(s) to display.</p>

<p><b>Note: you may</b> return to this page at any time and select other versions and/or
revert back to the "latest" (default) selection.</p>

<p><b>Note also: not</b> all plots may have been generated with all versions,
so when you select specific (especially older) versions, some curves may disappear.
This may also occur simply if some tunes are not available in all versions.
If you are sure that a certain curve <i>ought</i> to exist for a certain generator version
(i.e., it is there for the default version and should be available also for the older one)
and you are not seeing it here, send us a mail, and we may be able to generate some statistics for it.</p>

<?php
  // extract the query
  list( , $q_beamgroup, , , ,$q_gen_version, $q_valid_gtvr) = explode(",", $_GET["query"]);

  //get all generators and versions
  $query = mysql_query("SELECT DISTINCT generator, version FROM histograms WHERE type = 'mc' ORDER BY 1, 2 DESC");
  $generators = array();
  
  while ($row = mysql_fetch_row($query)) {
    $generator=$row[0];
    $version=$row[1];
    $generators[$generator][]=$version;
  }
  
  $q_gen_version=rawurldecode($q_gen_version);
  //get array of genenerators from URL
  $lurl2obj = unpackStr($q_gen_version);

  // vlist - list of versions
  // vadd - additional version
  function createList($vlist, $vadd)
  {
    $items = explode("--1", $vlist);
    $items[] = $vadd;
    
    // construct map[generator] = version
    $verlist = array();
    
    foreach ($items as $i) {
      $par = explode("~", $i);
      $gen = $par[0];
      $ver = (count($par) > 1) ? $par[1] : "";
      $verlist[$gen] = $ver;
    }
    
    $items2 = array();
    
    foreach ($verlist as $gen => $ver) {
      if ($ver != "")
        $items2[] = $gen . "~" . $ver;
    }
    
    return implode("--1", $items2);
  }
  
  echo "<table id=\"versions\">\n";
  echo "<tr>\n";
  echo "<th>Generator</th>\n";
  echo "<th>Version</th>\n";
  echo "</tr>\n";
  
  foreach (array_keys($generators) as $generator) {
    echo "<tr>\n";
    echo "<td>$generator</td>\n";
    echo "<td>\n";
    $selected = array_key_exists($generator, $lurl2obj) ? "" : "selected";
    $url = prepare_link(array("versions", $q_beamgroup, "", "", "", createList($q_gen_version, $generator), $q_valid_gtvr));
    echo "<span><a href=\"$url#versions\" class=\"$selected\" rel=\"nofollow\">latest</a></span>\n";
    
    foreach ($generators[$generator] as $version) {
      $selected = (array_key_exists($generator, $lurl2obj) && ($lurl2obj[$generator][0] == $version)) ? "selected" : "";
      $url = prepare_link(array("versions", $q_beamgroup, "", "", "", createList($q_gen_version, $generator . "~" . $version), $q_valid_gtvr));
      echo "<span><a href=\"$url#versions\" class=\"$selected\" rel=\"nofollow\">$version</a></span>\n";
    }
    
    echo "</td>\n";
    echo "</tr>\n";
  }
  
  echo "</table>\n";
?>

<h3>Version X has lousy statistics?</h3>
<p>See the <a href="http://lhcathome.web.cern.ch/projects/test4theory">Test4Theory project</a> 
for how to contribute to generating more statistics for mcplots.</p>

<h3>Missing a Generator/Version?</h3>
<p>See the <a href="?query=frontpage">Front Page</a> for possibilities to work
with us to add new plots / generators / versions / ... to the site.</p>

<h3>Feedback</h3>
<p>Please write to us if you are experiencing problems or unexpected behaviour
in connection with this page (or even if you are just plain happy with it).
The possibility to select generator versions
was first implemented by A. Pytel (CERN, March 2011).</p>
