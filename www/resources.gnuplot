# jobs run time
set terminal png size 650,230
set output "cache/stats/runtime.png"
set xlabel "Job run time, minutes"
set ylabel "#jobs (in 5 minutes bins)"
set grid xtics ytics
set xtics 60

# disable legend
unset key

# set size of bin boxes
set boxwidth 3

plot [0:300] "cache/stats/runtime.txt" using ($1+2.5):2 with boxes linetype 2 fs solid 0.3


# jobs disk usage
set terminal png size 650,230
set output "cache/stats/diskusage.png"
set xlabel "Job disk usage, megabytes"
set ylabel "#jobs (in 1 MB bins)"
set grid xtics ytics
set xtics autofreq

# disable legend
unset key

# set size of bin boxes
set boxwidth 0.7

plot [0:30] "cache/stats/diskusage.txt" using ($1+0.5):2 with boxes linetype 2 fs solid 0.3

