// following two function are intended to show/hide elements on index.php pages
// inspared by http://www.codeisart.ru/js-core-fast-dom-element-traversing/
function toggle(item)
{
  while (item = item.nextSibling)
    if (item.className == "submenu-items" || item.className == "submenu-items-hidden")
      break;

  if (item.className == "submenu-items")
    item.className = "submenu-items-hidden";
  else
    item.className = "submenu-items";
}

function togglespan(item)
{
  var mainspan = item;
  
  while (item = item.nextSibling)
    if (item.className == "caption-items" || item.className == "caption-items-hidden")
      break;
  
  if (item.className == "caption-items") {
    mainspan.innerHTML = "show details &rarr;";
    item.className = "caption-items-hidden";
  }
  else {
    mainspan.innerHTML = "hide details &larr;";
    item.className = "caption-items";
  }
}

// "sherpa 2.2.1 default" -> "sherpa default"
function gvt2gt(s)
{
  var items = s.split(" ");
  items.splice(1, 1);
  return items.join(" ");
}

// convenience for 'Custom' page
// select all generator versions for Alt+click on tune name
function customClick(event, item)
{
  if (!event) event = window.event;
  
  /*
  console.log("Clicked" +
              ", value = " + item.value +
              ", new state = " + item.checked +
              ", alt = " + event.altKey +
              ", ctrl = " + event.ctrlKey +
              ", meta = " + event.metaKey);
  */
  
  if (event.altKey) {
    var option0 = gvt2gt(item.value);
    //console.log("reference " + item.value + " -> " + option0);
    
    var menu = document.getElementById("custommenu");
    var rows = menu.getElementsByTagName('tr');
    
    var len = rows.length;
    
    for (var i = 0; i < len; i++) {
      var row = rows[i];
      //var generator = row.children[0].textContent;
      //var version = row.children[1].textContent;
      var tunes = row.children[2];
      
      for (var j = 0; j < tunes.children.length; j++) {
        var checkbox = tunes.children[j].children[0];
        var tune1 = checkbox.value;
        var option1 = gvt2gt(tune1);
        
        if (option1 == option0) {
          //console.log(tune1 + " -> " + option1);
          checkbox.checked = item.checked;
        }
      }
    }
  }
}

// navigate to a new URL
function navigate(url)
{
  window.location.href = url;
}

// Remove an element and provide a function that inserts it into its original position
// https://developers.google.com/speed/articles/javascript-dom
// section "Out-of-the-flow DOM Manipulation"
function removeToInsertLater(element) {
  var parentNode = element.parentNode;
  var nextSibling = element.nextSibling;
  parentNode.removeChild(element);
  return function() {
    if (nextSibling) {
      parentNode.insertBefore(element, nextSibling);
    } else {
      parentNode.appendChild(element);
    }
  };
}


// for production.php
function applyFilter()
{
  var term = document.getElementById("filterEdit").value;
  var invert = document.getElementById("invertFlag").checked;
  
  var runs = document.getElementById("runs");
  var insertFunction = removeToInsertLater(runs);
  var rows = runs.getElementsByTagName('tr');
  
  var matches = 0;
  var len = rows.length;
  
  for (var i = 1; i < len; i++) {
    var row = rows[i];
    var run = row.children[0].textContent;
    var match = (run.indexOf(term) !== -1);
    
    if (match != invert) { // logical XOR
      // TODO: highline 'term'
      row.style.display = "table-row";
      matches++;
    }
    else {
      row.style.display = "none";
    }
  }
  
  insertFunction();
  
  var state = document.getElementById("filterState");
  state.textContent = (term !== "") ? "Keyword: " + term + " (matched " + matches + " of " + (len-1) + " rows)" : "";
}
