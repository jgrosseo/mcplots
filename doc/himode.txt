Running the heavy-ion mode in MCPLOTS
---------------------------------------------

  Heavy-ion mode allows to run heavy-ion analyses in MCPLOTS. It requires some 
  extensions in the software so it does not use the official release of Rivet. 
  There are up to 3 steps to run the full analysis in heavy-ion mode. Currently 
  there are 3 analyses which require using the software with heavy-ion extensions:
  ALICE_2010_I880049: require calibration
  ALICE_2012_I939312: require calibration & postprocessing
  ALICE_2012_I1127397: require calibration & postprocessing


A. Software
---------------------------------------------
  For the heavy-ion analyses the official ALICE installation of Rivet containing the necessary 
  heavy-ion extensions will be used. Set the following variable:

  $ export HIMODE=1
  
  to run in the normal heavy-ion mode. You can also run the calibration mode by setting:

  $ export HIMODE=1
  $ export HICAL=1
  
  As a result, the heavy-ion version of software will be loaded automatically in case user 
  will try to run the heavy-ion analyses. The installation can be found here:
  
  /cvmfs/alice.cern.ch/el6-x86_64/Packages/Rivet-hi/2.6.0-alice1-3
  

B. How to run a full chain of HI analyses
---------------------------------------------

 -> Produce calibration files
  
    Run the heavy-ion analyses in the calibration mode without providing any calibration histograms. 
    Example:
  
    $ export HIMODE=1
    $ export HICAL=1
    $ cd /home/mcplots/scripts/mcprod/
    $ ./runRivet.sh local PbPb heavyion-mb 2760 - - epos 1.99.crmc.1.5.4-hi lhc 10k 123
  
    The number of the events provided should be equal or greater than required number of events to 
    prepare the calibration files. In that case each analysis will produce the output which 
    consist of the calibration histograms required for this analysis. If the number of events 
    is greater than required for the calibration histogram then the analysis will automatically 
    switch into the normal mode and continue with the remaining number of events filling regular analysis 
    histograms together with the control histograms instead of calibration. After the run finishes 
    calibration and control histograms will be automatically merged and placed in the corresponding directory:

    /home/mcplots/scripts/mcprod/calibration/<beam>/<process>/<energy>/<generator>/<version>/<tune>/<analysis_name>.yoda

    Repeat this step for all beams/energies/etc. to have the calibration histograms available for each heavy-ion analysis.

 -> Run the analysis

    After collecting all the calibration files required run the heavy-ion analysis in the normal mode, e.g:

    $ export HIMODE=1
    $ export HICAL=0
    $ cd /home/mcplots/scripts/mcprod/
    $ ./runRivet.sh local PbPb heavyion-mb 2760 - - epos 1.99.crmc.1.5.4-hi lhc 100k 123
  
    It will read in all the required calibration histograms stored in the calibration/ directory automatically 
    and proceed with the normal main analysis mode. If there are missing calibration histograms (or not enough entries in
    the histograms) the analysis will produce them using the first N required events to get the proper calibration plots 
    and it will proceed with the normal analysis mode. The calibration and control histograms which will be produced during 
    this run will not be merged and will not be saved in the calibration/ directory.

 -> Run postprocessing

    If the analysis requires postprocessing to e.g. create ratios of histograms for different runs, then the 
    additional postprocessing is required to produce final histograms. To produce the final plots for all 
    analyses which require postprocessing simply run:

    $ export HIMODE=1
    $ export HICAL=0
    $ cd /home/mcplots/scripts/mcprod/
    $ ./runPost.sh all
  
    It will read in the input yoda files from the previous runs stored in /home/mcplots/scripts/mcprod/YODAIO directory 
    and perform the postprocessing for all files available in this directory.
